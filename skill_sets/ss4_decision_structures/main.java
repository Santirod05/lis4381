class main
{
    public static void main(String args[])
    {
        // call static void methods (i.e. no object, non-value returning)
        methods.getRequirements();
        methods.getUserPhoneType();
    }
}