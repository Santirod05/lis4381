import java.util.Scanner;

public class methods
{

    public static void getRequirements()
    {
        // Display operational messages
        System.out.println("Developer: Rafael S. Rodriguez");
        System.out.println("Program evaluates integers as even or odd.");
        System.out.println("Note: Program does *not* check for non-numeric characters");
        System.out.println(); // prints blank line
    }

        public static void evaluateNumber()
        {
            // initialize variables, create Scanner object, capture user input
            int x = 0;

            System.out.print("Enter integer: ");
            Scanner sc = new Scanner(System.in);
            x = sc.nextInt();

                if ( x % 2 == 0)
                    {
                        System.out.println( x + " is an even number.");
                    }
                else 
                    {
                        System.out.println( x + " is an odd number.");
                    }
        }
}
    