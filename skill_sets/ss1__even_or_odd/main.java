public class main
{
    public static void main(String args[])
    {
        // call static methods (i.e., no object)
        methods.getRequirements();
        methods.evaluateNumber();
    }
}