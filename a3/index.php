<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Rafael S. Rodriguez">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment 2</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<style>
		html
		{
			height: 100%;
		}
			body 
		{
			background-image: linear-gradient(#6D0200, #f2fbff, #6D0200);
				background-image: linear-gradient(to right, #6D0200, #f2fbff, #6D0200);
		}

		div
		{
			color: #000;
		}
		</style>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> Create ERD based upon business rules, <br>
					Provide Screenshots of 10 records for each table, <br>
					Provide Screenshot of running application’s opening user interface, <br>
					Provide Screenshot of running application’s processing user input, <br>
					Provide Screenshots of skillsets 4-6. <br>
				</p>

				<h4>Screenshot of a3 ERD </h4>
				<img src="img/a3_erd.png" class="img-responsive center-block" alt="Screenshot of a3 ERD">

				<h3>Screenshot of 10 records for each a3 populated tables</h3>
				<h4>Screenshot of table 1</h4>
				<img src="img/cus.png" class="img-responsive center-block" alt="Screenshot of table 1">

				<h4>Screenshot of table 2</h4>
				<img src="img/pet.png" class="img-responsive center-block" alt="Screenshot of table 2">

				<h4>Screenshot of table 3</h4>
				<img src="img/pst.png" class="img-responsive center-block" alt="Screenshot of table 3">

				<h4>Screenshot of running application’s opening user interface</h4>
				<img src="img/app.png" class="img-responsive center-block" alt="Screenshot of table 1">

				<h4>Screenshot of running application’s  processing user input</h4>
				<img src="img/app2.png" class="img-responsive center-block" alt="Screen shot of skillset 1">

				<h3>Screenshot of Skillset #4 - #6:</h3>

				<h4>Screenshot of skillset 4</h4>
				<img src="img/ss4.png" class="img-responsive center-block" alt="Screen shot of skillset 4">

				<h4>Screenshot of skillset 5</h4>
				<img src="img/ss5.png" class="img-responsive center-block" alt="Screen shot of skillset 5">

				<h4>Screenshot of skillset 6</h4>
				<img src="img/ss6.png" class="img-responsive center-block" alt="Screen shot of skillset 6">

				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
