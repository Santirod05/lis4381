> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 MOBILE WEB APPLICATION DEVELOPMENT.

## Rafael Rodriguez

### Assignment #3 Requirements:

*Deliverables:*

*Four Parts:*

1. Screenshot of Pet Store ERD
2. Screenshot of Concert Ticket first and second user interface
3. Screen shots of 10 records from each Pet Store table
4. Links to a3.mwb and a3.sql

#### README.md file should include the following items:

* Screenshot of a3 ERD;
* Screenshots of 10 records for each table;
* Screenshot of running application’s opening user interface;
* Screenshot of running application’s processing user input;
* Screenshots of skillsets 4-6
* Links to the following files:
    * Link to a3.mwb
    [a3.mwb](docs/a3.mwb)
    * Link to a3.sql
    [a3.sql](docs/a3.sql)



> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

#### Screenshot of a3 ERD

![a3 ERD](img/a3_erd.png)

#### Screenshot of 10 records for each a3 populated tables

| Table 1 | Table 2 | Table 3 |
| -------| ----------- | ----------- |
| ![Table 1](img/cus.png) | ![Table 2](img/pet.png) | ![Table 3](img/pst.png)

#### Screenshot of running application’s opening user interface & processing user input

| opening user interface | rocessing user input |
| -------- | -------- |
|![Android Studio "opening interface" Screenshot 1](img/app.png) | ![Android Studio "processing user input" Screenshot 2](img/app2.png) |


#### Skillset #4 - #6:

| Skillset #4 | Skillset #5 | Skillset #6|
| -------| ----------- | ----------- |
| ![Skillset #4](img/ss4.png) | ![Skillset #5](img/ss5.png) | ![Skillset #6](img/ss6.png)

