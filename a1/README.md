> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Mobile Web Application Development

## Rafael Rodriguez

### Assignment #1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket.
2. Develop Installations.
3. Chapter Questions (Chapters 1-2)

#### README.md file should include the following items:

* Screenshot of AMPPS Installation My PHP installation
* Screenshot of running java Hello:
* Screenshot of running Android studio - Hello World App 
* Git commands with short descriptions:
* Bitbucket repo links: 
    * a) This assignment and
    * b) the completed tutorial(bitbucketstationlocations)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git rm - Remove files from the working tree and from the index

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:
![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of MySQL root user running*:
![Screenshot of Mysql running on terminal](img/mysql_run.png "Screenshot of MySQL running.")

*Screenshot of PHP info running http://localhost:8080/phpinfo.php*:
![AMPPS Installation Screenshot](img/php_info.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of running Android studio - Hello World App*:

![running Android studio Screenshot](img/android.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Santirod05/LIS4368/ "Bitbucket Station Locations")
