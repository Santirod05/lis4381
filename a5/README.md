> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development.

## Rafael Rodriguez

### Assingment 5 Requirements:

*Deliverables:*
*Three Parts:*

1. Create Connection to Local Host database to perform server side validation on data entries
2. modifiy previous Assignment 4 files (meta tags, navigation links, titles, etc) to fit these Requirements.
3. Skill set 13-15

#### README.md file should include the following items:
 
- Assignment requirements.
- Screenshot of Assignment 5 index.php 
- Screenshot of add_petstore.php (invalid).
- Screenshot of add_petstore_process.php (Failed Validation)
- Screenshot of add_petstore.php (valid)
- Screenshot of add_petstore_process.php (Passed Validation)
- Screenshots of Skillsets 13-15:
    - SS13 - Sphere Volume Calculator
    - SS14 - Simple Calculator
    - SS15 - Write Read File
    
#### Assignment Screenshot and Links:

#### Screenshot of Assignment 5 index.php

|  Assignment 5 index.php | 
| ----------------------- |
|![Screenshot of Assignment 5 index.php](img/index.png)|

#### Failed Validation Screenshots:

| Client Side Validation |  Server Side Validation Error |
| ---------------------- | ----------------------------- |
| ![Screenshot of invalid data](img/add_invalid.png) | ![A2 Screenshot 2](img/failed_val.png) |

#### Passed Validation Screenshots:

| Correct Data vailidation |  Server Side Passed Validation | 
| ------------------------ | ----------------------- | 
| ![Screenshot of valid data](img/add_valid.png) | ![Screenshot of passed validation](img/passed_val.png) |


#### Skillset 13 Sphere Volume Calculator Screenshots

|  *Skillset 13*: | 
|-----------------|
|![Screenshot of Skillset 13](img/ss13.png)|

#### Skillset 14 Simple Calculator Screenshots

|  *Addition (HOME)*: | *Addition (RESULT)*: |
| ------------------- | -------------------- |
|![Screenshot of Skillset 14](img/addition.png)|![Skillshot 14 addition results](img/addition2.png)|

|  *Subtraction (HOME)*: | *Subtraction (RESULT)*: |
| ---------------------- | ----------------------- |
|![Screenshot of Skillset 14](img/subtract.png)|![Skillshot 14 addition results](img/subtract2.png)|

| *Multiplication (HOME)*: | *Multiplication (RESULT)*: |
| ------------------------ | -------------------------- |
|![Screenshot of Skillset 14](img/multiply.png)|![Skillshot 14 division result](img/multiply2.png)|

| *Division (HOME)*: | *Division (RESULT)*: |
| ------------------ | -------------------- |
|![Screenshot of Skillset 14](img/division.png)|![Skillshot 14 division result](img/division2.png)|


#### Skillset 15 Write Read File Screenshots

|  *Skillset 15 (HOME)*: | *Skillset 15 (RESULT)*: |
|-----------------|----------------|
|![Screenshot of Skillset 15](img/filedata.png)|![Screenshot of Skillset 15](img/filedata2.png)|


[Link to website](http://localhost:8080/lis4381/index.php)

