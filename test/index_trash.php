<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
		<title>Using RSS Feeds / Project 1</title>		
		
		<style>
		html]
		{
			height: 100%;
		}
			body 
		{
			background-image: linear-gradient(#6D0200, #f2fbff, #6D0200);
				background-image: linear-gradient(to right, #6D0200, #f2fbff, #6D0200);
		}

		div
		{
			color: #000;
		}
		</style>
  </head>
  <body>
	 <?php
	 //Note: RSS specification: hhtps://validator.w3.org/feed/docs/rss2.html

		$html = "";
		$publisher = "Fox";
		$url = "https://feeds.foxnews.com/foxnews/latest";

		$html .= '<h2>' . $publisher . '</h2>';
		$html .= $url;

		$rss = simplexml_load_file($url);
		$count = 0;
		$html .= '<ul>';

		foreach($rss->channel->item as $item)
		{
			$count++;
			if($count > 10)
			{
				break;
			}
			$html .= '<li><a href="' . htmlspecialchars($item->link).'">' . htmlspecialchars($item->title) . '</a><br />';
			$html .= htmlspecialchars($item->description) . '<br />';
			$html .= htmlspecialchars($item->pubDate) . '</li><br />';
		}
		$html .= '</ul>';

		print $html;
	?>

    </body>

</html>