<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Using RSS Feeds</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
        <meta name="author" content="Rafael S. Rodriguez">
        <link rel="icon" href="favicon.ico">
		<?php include_once("../css/include_css.php"); ?>

        <style>
        html
        {
            height: 100%;
        }
            body 
        {
            background-image: linear-gradient(#6D0200, #f2fbff, #6D0200);
                background-image: linear-gradient(to right, #6D0200, #f2fbff, #6D0200);
        }

        div
        {
            color: #000;
        }
        </style>

	</head>
<body>
	<?php

	$html = "";
	$publisher = "BBC";
	$url = "http://feeds.bbci.co.uk/news/technology/rss.xml";

	$html .='<h2>' .$publisher .'</h2>';
	$html .=$url;

	$rss = simplexml_load_file($url);
	$count = 0;
	$html .='<ul>';
	foreach($rss->channel->item as $item)
	{
		$count++;
		if($count > 10)
		{
			break;
		}
		$html .='<li><a href="' . htmlspecialchars($item->link). '">'.htmlspecialchars($item->title).'</a><br/>';
		$html .= htmlspecialchars($item->description).'<br/><br/>';
		$html .= htmlspecialchars($item->pubDate).'<br/>';
	}
	$html .= '</ul>';

	print $html;
?>

</body>
</html>
