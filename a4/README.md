> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 MOBILE WEB APPLICATION DEVELOPMENT.

## Rafael Rodriguez

### Assignment #4 Requirements:

*Deliverables:*

*Four Parts:*

1. Create Online Portfolio using Apache and Bootstrap
2. Use jQuery to validate client side data
3. Link to local host web app
4. Skill Set 10-12

#### README.md file should include the following items:

* Screenshot of LIS4381 Portal (Main Page)
* Screenshot of Failed Basic client-side validation 
* Screenshot of Passed Basic client-side validation
* Screenshots of skillsets 10-12


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

#### Screenshot of LIS4381 Portal (Main Page)
![LIS4381 Portal (Main Page)](img/main.png)

#### Screenshot of Failed and Passed Basic client-side validation

| Table 1 | Table 2 |
| ------- | ----------- |
![Failed Basic client-side validation Screenshot](img/failed_val.png)| ![Table 2](img/passed_val.png) |


#### Skillset #10 - #12:

| Skillset #10 | Skillset #11 | Skillset #12 |
| -------| ----------- | ----------- |
| ![Skillset #4](img/ss10.png) | ![Skillset #5](img/ss11.png) | ![Skillset #6](img/ss12.png)
