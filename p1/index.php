<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Rafael S. Rodriguez">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Project 1</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<style>
		html
		{
			height: 100%;
		}
			body 
		{
			background-image: linear-gradient(#6D0200, #f2fbff, #6D0200);
				background-image: linear-gradient(to right, #6D0200, #f2fbff, #6D0200);
		}

		div
		{
			color: #000;
		}
		</style>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> Create My Business Card Application using Android Studio <br>
					Includes Launcher Icon, background color and images. <br>
					Skill Set 7-9
				</p>

				<h4>Screenshot of My Business Card Application using Android Studio - part 1</h4>
				<img src="img/business_info1.png" class="img-responsive center-block" alt="Screenshot of My Business Card App 1">

				<h4>Screenshot of My Business Card Application using Android Studio - part 2</h4>
				<img src="img/business_info2.png" class="img-responsive center-block" alt="Screenshot of My Business Card App 2">

				<h3>Screenshot of Skillsets 7 - 9</h3>

				<h4>Skillset 7</h4>
				<img src="img/ss7.png" class="img-responsive center-block" alt="Screenshot of Skillsets 7">

				<h4>Skillset 8</h4>
				<img src="img/ss8.png" class="img-responsive center-block" alt="Screenshot of Skillsets 8">

				<h4>Skillset 9</h4>
				<img src="img/ss9.png" class="img-responsive center-block" alt="Screenshot of Skillsets 9">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
