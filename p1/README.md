> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 MOBILE WEB APPLICATION DEVELOPMENT.

## Rafael Rodriguez

### Project #1 Requirements:

*Deliverables:*

*Four Parts:*

1. Create My Business Card Application using Android Studio
2. Includes Launcher Icon, background color and images.
3. Skill Set 7-9

#### README.md file should include the following items:

* Screenshot of My Business Card Application using Android Studio;
* Screenshots of skillsets 7-9


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

#### Screenshot of My Business Card Application using Android Studio

| Table 1 | Table 2 |
| ------- | ----------- |
![p1 Screenshot](img/business_info1.png)| ![Table 2](img/business_info2.png) |


#### Skillset #7 - #9:

| Skillset #7 | Skillset #8 | Skillset #9 |
| -------| ----------- | ----------- |
| ![Skillset #4](img/ss7.png) | ![Skillset #5](img/ss8.png) | ![Skillset #6](img/ss9.png)
