> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development.

## Rafael Rodriguez

### Project 2 Requirements:

*Deliverables:*
*Three Parts:*

1. Update previous Assignment 4 and 5 to continue C.R.U.D. protocol.
2. Project 2 works on Update and Deleting Records from database
3. Includes RSS Feed of creators chosing.

#### README.md file should include the following items:
 
- Assignment requirements.
- Screenshot of Carousel (Home page –include images, self-promotional links)
- Screenshot of index.php page
- Screenshot of edit_petstore.php
- Screenshot of edit_petstore.php Failed Validation
- Screenshot of edit_petstore.php (Passed Validation)
- Screenshots of Delete Record Prompt
- Screenshots of Successfully Deleted Record
- Screenshots of RSS Feed (Link to RSS feed of your choice)
    
#### Assignment Screenshot and Links:

#### Carousel (Home page – include images, self-promotional links) :

|  Home Page Carousel | 
| ----------------------- |
|![Screenshot of Home Page Carousel](img/main.png)|

#### Project 2 index.php page :

|  Project 2 | 
| ----------------------- |
|![Screenshot of Project 2 index.php](img/p2.png)|

#### edit_petstore.php passed and failed validation :

| edit_petstore.php |  Failed Validation | Passed Validation |
| ---------------------- | ----------------------------- | ----------------------------- |
| ![Screenshot of edit_petstore](img/edit.png) | ![edit_petstore failed](img/edit_failed.png) | ![edit_petstore pass](img/edit_pass.png) |

#### Delete Record :

| Delete Record Prompt |  Successfully Deleted Record | 
| ------------------------ | ----------------------- | 
| ![Screenshot of  Delete Record Prompt](img/delete.png) | ![Screenshot of Successfully Deleted Record](img/delete_pass.png) |


#### RSS Feed :

|  Rss Feed : | 
|-----------------|
|![Screenshot of Rss Feed](img/rss.png)|


[Link to website](http://localhost:8080/lis4381/index.php)

