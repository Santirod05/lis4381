<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements for my Mobile Web Application Development.">
		<meta name="author" content="Rafael S. Rodriguez">
		<link rel="icon" href="favicon.ico">

		<title>Creative Online Portfolio</title>

		<?php include_once("css/include_css.php"); ?>	

		<!-- Carousel styles -->
		<style type="text/css">
			html
			{
				height: 100%;
			}
			body 
			{
			
				background-image: linear-gradient(to bottom right, #6D0200, #f2fbff, #6D0200);
				background-image: linear-gradient(to right, #6D0200, #f2fbff, #6D0200);
			}

		 h2
		 {
			 margin: 0;     
			 color: #435483;
			 padding-top: 50px;
			 font-size: 53px;
			 font-family: "trebuchet ms", sans-serif;    
		 }
		 h3
		 {
			 text-shadow: 1px 1px #000000;
			 color: #982932;
		 }
		 h4
		 {
			padding: 30px;
			text-shadow: 1px 1px #000000;
			font-size: 25px;
			 color: #982932;
		 }
		 h5
		 {
			text-shadow: 2px 1px #000000;
			font-size: 25px;
			 color: #982932; 
		 }

        p
        {
            text-shadow: 1px 1px #000000;
			 color: #982932;
        } 

		 a.Info
		 {
			margin-Bottom: 50px;
			-webkit-appearance: button;
			 -moz-appearance: button;
			 appearance: button;
			 
			 padding: 5px;
			 color: #982932;
		 }
		 .item
		 {
			 background: #333;   
			 text-align: center;
			 height: 400px !important;
		 }
		 .carousel
		 {
			 margin: 20px 0px 20px 0px;
		 }
		 .bs-example
		 {
			 margin: 20px;
		 }
		</style>

	</head>
	<body>

		<?php include_once("global/nav_global.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>

				<!-- Start Bootstrap Carousel  -->
				<div class="bs-example">
					<div
						id="myCarousel"
								class="carousel"
								data-interval="3000"
								data-pause="hover"
								data-wrap="true"
								data-keyboard="true"			
								data-ride="carousel">
						
    				<!-- Carousel indicators -->
						<ol class="carousel-indicators">
							<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
							<li data-target="#myCarousel" data-slide-to="1"></li>
							<li data-target="#myCarousel" data-slide-to="2"></li>
						</ol>   
						<!-- Carousel items -->
						<div class="carousel-inner">

							
							<div class="active item" style="background: url(img/bitbucket.png); background-size: cover;">
								<div class="container">
									<div class="carousel-caption">
										<h3>Bitbucket Repository with Projects</h3>
										<a href="https://bitbucket.org/Santirod05/" class="Info">Click for Info</a>
										<p>Bitbucket Profile where all four of my repositories can be accessed for different courses taken at FLorida State University.</p>
                        </div>
                      </div>
                    </div>
              
							<div class="item" style="background: url(img/linkedin.png); background-size: cover;">
								<div class="carousel-caption">
									<h3>Linkedin: Rafael S. Rodriguez</h4>
									<a href="https://www.linkedin.com/in/rafael-rodriguez-0a6419206/" class="Info">Click for Info</a>
									<p>Linked in profile where one can network with me about future job oportunities.</p>								
								</div>
							</div>

							<div class="item" style="background: url(img/p1_erd.png); background-size: cover;">
								<div class="carousel-caption">
									<h4>Database in order to track and document the city’s court case data</h5>
									<p>
									An attorney is retained by (or assigned to) one or more clients, for each case. 
									• A client has (or is assigned to) one or more attorneys for each case.
									• An attorney has one or more cases.
									• A client has one or more cases.
									• Each court has one or more judges adjudicating.
									• Each judge adjudicates upon exactly one court.
									• Each judge may preside over more than one case.
									• Each case that goes to court is presided over by exactly one judge.
									</p>
									<a href="https://bitbucket.org/Santirod05/lis3781/src/master/" class="Info">Click for Info</a>							
								</div>
							</div>

						</div>
						<!-- Carousel nav -->
						<a class="carousel-control left" href="#myCarousel" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left"></span>
						</a>
						<a class="carousel-control right" href="#myCarousel" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right"></span>
						</a>
					</div>
				</div>
				<!-- End Bootstrap Carousel  -->
				
				<?php
				include_once "global/footer.php";
				?>

			</div> <!-- end starter-template -->
    </div> <!-- end container -->

		<?php include_once("js/include_js.php"); ?>	
	  
  </body>
</html>
