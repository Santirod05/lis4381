> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions.

## Rafael Rodriguez

### Assignment #2 Requirements:

*Deliverables:*
*Four Parts:*

1. *Two Parts*
1.   Screenshots of Android Studio "Healthy Recipe" App
2.   Skill Sets 1-3

#### README.md file should include the following items:

* Screenshot of payroll_calculator program without overtime
* Screenshot of payroll_calculator program with overtime
* Screenshot of skillsets #1-3
* Jupyter notebook of payroll_calculator

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of Android Studio - Healthy Recipe App*:

| Screen 1 | Screen 2 |
| -------- | -------- |
|![Android Studio "Healthy Recipe App" Screenshot 1](img/app.png) | ![Android Studio "Healthy Recipe App" Screenshot 2](img/app2.png) |


#### Skillset #1 - #3:

| Skillset #1 | Skillset #2 | Skillset #3 |
| -------| ----------- | ----------- |
| ![Skillset #1](img/ss1.png) | ![Skillset #2](img/ss2.png) | ![Skillset #3](img/ss3.png)

