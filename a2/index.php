<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Rafael S. Rodriguez">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment 2</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<style>
		html
		{
			height: 100%;
		}
			body 
		{
			background-image: linear-gradient(#6D0200, #f2fbff, #6D0200);
				background-image: linear-gradient(to right, #6D0200, #f2fbff, #6D0200);
		}

		div
		{
			color: #000;
		}
		</style>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> Create Healthy Recepies Android App <br>
						Provide screenshots of Android App and Instructions <br>
						Provide screenshots of completed app 
				</p>

				<h4>Screenshot of Android Studio - Healthy Recipe App: Part 1 </h4>
				<img src="img/app.png" class="img-responsive center-block" alt="Screenshot of Android Studio">

				<h4>Screenshot of Android Studio - Healthy Recipe App: Part 2</h4>
				<img src="img/app2.png" class="img-responsive center-block" alt="Screenshot of Android Studio">

				<h4>Screenshot of skillset 1</h4>
				<img src="img/ss1.png" class="img-responsive center-block" alt="Screen shot of skillset 1">

				<h4>Screenshot of skillset 2</h4>
				<img src="img/ss2.png" class="img-responsive center-block" alt="Screen shot of skillset 2">

				<h4>Screenshot of skillset 3</h4>
				<img src="img/ss3.png" class="img-responsive center-block" alt="Screen shot of skillset 3">

				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
