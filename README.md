> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

#  LIS4381 MOBILE WEB APPLICATION DEVELOPMENT.  

## Rafael Rodriguez

### Assignment Requirements:

*Assignment Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Install JDK
    - Install Android Studio
    - Provide screenshots of Installations
    - Create Bitbucket Repo
    - Complete Bitbucket tutorials (bitbucketlocations)
    - Provide git command descriptions
2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create Healthy Recepies Android App
    - Provide screenshots of Android App and Instructions
    - Provide screenshots of completed app
3. [A3 README.md](a3/README.md "My A3 README.md file")
	- Create ERD based upon business rules 
    - Provide Screenshots of 10 records for each table;
    - Provide Screenshot of running application’s opening user interface;
    - Provide Screenshot of running application’s processing user input;    
    - Provide Screenshots of skillsets 4-6
4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Create Online Portfolio using Apache and Bootstrap
    - Use jQuery to validate client side data
    - Link to local host web app
    - Skill Set 10-12

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Create Connection to Local Host database to perform server side validation on data entries
    - modifiy previous Assignment 4 files (meta tags, navigation links, titles, etc) to fit these Requirements.
    - Skill set 13-15
    
### Project Requirements:

*Project Links:*

1. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create My Business Card Application using Android Studio
    - Includes Launcher Icon, background color and images.
    - Skill Set 7-9

2. [P2 README.md](p2/README.md "My P2 README.md file")
    - Update previous Assignment 4 and 5 to continue C.R.U.D. protocol.
    - Project 2 works on Update and Deleting Records from database
    - Includes RSS Feed of creators chosing.
